<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Lab 4</title>
</head>

<body>

    <?php
	    if (!empty($result_message)) {
	        print('<div id="messages">');
	        foreach ($result_message as $message) {
		        print($message);
	        }
	        print('</div>');
	      }
    ?>

    <div class="main">
    <div class="block_4">
        <h1>Form</h1>
        <form method="POST" id="form" action="index.php">
            <label>Name:<br />
                <input type=text name="field-name" placeholder="Input your name" 
                <?php if($errors['field-name']){print 'class = "error"';}?> 
                value="<?php print $values['field-name']; ?>" />
            </label><br />

            <label>E-mail:<br />
                <input name="field-email" placeholder="Input your e-mail" type="email"
                <?php if($errors['field-email']){print 'class = "error"';}?>
                value="<?php print $values['field-email']; ?>">
            </label><br />

            <label>Date of birth:<br />
            <input type="date" name="field-date" <?php if($errors['field-date']){print 'class = "error"';}?>  value="<?php print $values['field-date']; ?>">
            </label><br />

            <label>Gender:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-sex" value="M" <?php if ($values['radio-sex'] == "M") {print 'checked';} ?> />Male
            </label>
            <label class="radio"><input type="radio" name="radio-sex" value="F" <?php if ($values['radio-sex'] == "F") {print 'checked';} ?>/>Female
            </label><br />

            <label>Number of limbs:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="radio-limb" value=0 <?php if ($values['radio-limb'] == '0') {print 'checked';} ?>/>0
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=1 <?php if ($values['radio-limb'] == '1') {print 'checked';} ?>/>1
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=2 <?php if ($values['radio-limb'] == '2') {print 'checked';} ?>/>2
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=3 <?php if ($values['radio-limb'] == '3') {print 'checked';} ?>/>3
            </label>
            <label class="radio"><input type="radio" name="radio-limb" value=4 <?php if ($values['radio-limb'] == '4') {print 'checked';} ?>/>4
            </label><br />

            <label>Superpowers:<br />
                <select multiple="true" name="superpower[]" <?php if($errors['superpower']){print 'class = "error"';}?> >
                    <option value="1" <?php if ($values['superpower']['0']) {print 'selected';} ?>>Immortality</option>
                    <option value="2" <?php if ($values['superpower']['1']) {print 'selected';} ?>>Fire balss</option>
                    <option value="3" <?php if ($values['superpower']['2']) {print 'selected';} ?>>Flying</option>
                </select>
            </label><br />

            <label>
                Biography:<br />
                <textarea name="BIO" placeholder="Tell about yourself"></textarea>
                <br />
            </label>

            <label <?php if ($errors['ch']) {print 'class="error"';} ?>>
                <input name="ch" type="checkbox" checked=checked value=1>I have read the contract:<br />
            </label>

            <input type="submit" value="Send" />
        </form>
    </div>
</div>
</div>
   
</body>

</html>

